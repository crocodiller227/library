<?php

namespace AppBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;



/**
 * Reader
 *
 * @ORM\Table(name="reader")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\ReaderRepository")
 * @UniqueEntity("passport_id", message="Такое значение уже имеется | This value already exists")
 */
class Reader
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="full_name", type="string", length=255)
     * @Assert\NotBlank()
     */
    private $fullName;

    /**
     * @var string
     *
     * @ORM\Column(name="address", type="string", length=255)
     * @Assert\NotBlank()
     */
    private $address;

    /**
     * @var string
     *
     * @ORM\Column(name="passport_id", type="string", length=255, unique=true)
     * @Assert\NotBlank()
     */
    private $passportId;

    /**
     * @var string
     *
     * @ORM\Column(name="reader_card_id", type="string", length=255, unique=true)
     */
    private $readerCardId;

    /**
     * @var ArrayCollection
     * @ORM\OneToMany(targetEntity="Application", mappedBy="reader")
     */
    private $applications;



    public function __construct()
    {
        $this->applications = new ArrayCollection();
    }

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set fullName
     *
     * @param string $fullName
     *
     * @return Reader
     */
    public function setFullName($fullName)
    {
        $this->fullName = $fullName;

        return $this;
    }

    /**
     * Get fullName
     *
     * @return string
     */
    public function getFullName()
    {
        return $this->fullName;
    }

    /**
     * Set address
     *
     * @param string $address
     *
     * @return Reader
     */
    public function setAddress($address)
    {
        $this->address = $address;

        return $this;
    }

    /**
     * Get address
     *
     * @return string
     */
    public function getAddress()
    {
        return $this->address;
    }

    /**
     * Set passportId
     *
     * @param string $passportId
     *
     * @return Reader
     */
    public function setPassportId($passportId)
    {
        $this->passportId = $passportId;

        return $this;
    }

    /**
     * Get passportId
     *
     * @return string
     */
    public function getPassportId()
    {
        return $this->passportId;
    }

    /**
     * @param string $readerCardId
     * @return Reader
     */
    public function setReaderCardId($readerCardId)
    {
        $this->readerCardId = $readerCardId;
        return $this;
    }

    /**
     * @return string
     */
    public function getReaderCardId()
    {
        return $this->readerCardId;
    }

    /**
     * @param Application $application
     * @return Reader
     */
    public function addApplications($application)
    {
        $this->applications->add($application);
        return $this;
    }

    /**
     * @return ArrayCollection
     */
    public function getApplications()
    {
        return $this->applications;
    }
}

