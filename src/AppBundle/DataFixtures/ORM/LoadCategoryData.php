<?php

namespace AppBundle\DataFixtures\ORM;

use AppBundle\Entity\Category;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;

class LoadCategoryData extends Fixture implements DependentFixtureInterface
{

    public const CATEGORY = 'category';

    /**
     * Load data fixtures with the passed EntityManager
     *
     * @param ObjectManager $manager
     * @throws \Doctrine\Common\DataFixtures\BadMethodCallException
     */
    public function load(ObjectManager $manager)
    {
        $books = [];
        for ($i = 0; $i < 10; $i++) {
            $books[] = $this->getReference(LoadBookData::BOOK . $i);
        }
        $categories_name = ['Детектив', 'Проза', 'Фэнтези', 'Научпоп', 'Детям'];
        foreach ($categories_name as $key => $category_name){
            $category = new Category();
            $category
                ->setCategory($category_name)
                ->addBooks($books[array_rand($books)]);
            $manager->persist($category);
            $this->addReference(self::CATEGORY . $key, $category);
        }
        $manager->flush();
    }

    public function getDependencies()
    {
        return array(
            LoadBookData::class,
        );
    }
}