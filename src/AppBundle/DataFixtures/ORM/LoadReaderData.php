<?php

namespace AppBundle\DataFixtures\ORM;

    use AppBundle\Entity\Reader;
    use Doctrine\Bundle\FixturesBundle\Fixture;
    use Doctrine\Common\Persistence\ObjectManager;

class LoadReaderData extends Fixture
{

    public const READER = 'reader';

    /**
     * Load data fixtures with the passed EntityManager
     *
     * @param ObjectManager $manager
     * @throws \Doctrine\Common\DataFixtures\BadMethodCallException
     */
    public function load(ObjectManager $manager)
    {
        for ($i = 0; $i < 6; $i++) {
            $reader = new Reader();
            $reader
                ->setFullName("Reader{$i}")
                ->setAddress("Bishkek, 12 microdistrct, " . rand(1, 100) . ', ' . rand(0, 150))
                ->setPassportId('AN' . rand(11111, 999999))
                ->setReaderCardId(uniqid());
            $manager->persist($reader);
            $this->addReference(self::READER . $i, $reader);
        }
        $manager->flush();
    }
}
