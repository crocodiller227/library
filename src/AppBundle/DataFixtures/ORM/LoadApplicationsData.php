<?php

namespace AppBundle\DataFixtures\ORM;

use AppBundle\Entity\Application;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;

class LoadApplicationsData extends Fixture implements DependentFixtureInterface
{
    /**
     * This method must return an array of fixtures classes
     * on which the implementing class depends on
     * @return array []
     */
    function getDependencies()
    {
        return [
            LoadBookData::class,
            LoadReaderData::class
        ];
    }

    /**
     * Load data fixtures with the passed EntityManager
     *
     * @param ObjectManager $manager
     */
    public function load(ObjectManager $manager)
    {
        $books = [];
        for ($i = 0; $i < 10; $i++) {
            $books[] = $this->getReference(LoadBookData::BOOK . $i);
        }
        $readers = [];
        for ($i = 0; $i < 6; $i++) {
            $readers[] = $this->getReference(LoadReaderData::READER . $i);
        }

        for ($i = 0; $i< rand (2, 15); $i++){
            $application = new Application();
            $reg_date =rand(time() - 5 * 365 * 24 * 60 * 60, time());
            $comp_date = rand(time() - 365 * 24 * 60 * 60, time() + 365 * 24 * 60 * 60);
            if ($comp_date < time()){
                $status = 'Просрочено';
            }elseif ($comp_date > time()){
                $status = 'Ожидает';
            }
            $application
                ->setRegistrationDate(new \DateTime( date("Y-m-d H:i:s", $reg_date)))
                ->setCompletionDate(new \DateTime(date("Y-m-d H:i:s", $comp_date)))
                ->setStatus($status)
                ->setBook($books[array_rand($books)])
                ->setReader($readers[array_rand($readers)]);
            $manager->persist($application);
        }
        $manager->flush();
    }
}