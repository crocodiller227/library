<?php

namespace AppBundle\DataFixtures\ORM;

use AppBundle\Entity\Book;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;

class LoadBookData extends Fixture
{

    public const BOOK = 'book';

    /**
     * Load data fixtures with the passed EntityManager
     *
     * @param ObjectManager $manager
     * @throws \Doctrine\Common\DataFixtures\BadMethodCallException
     */
    public function load(ObjectManager $manager)
    {
        for ($j = 0; $j < 10; $j++) {
            $book = new Book;
            $book
                ->setAuthor("Author{$j}")
                ->setBook("Book {$j}")
                ->setCover("book.png");
            $manager->persist($book);
            $this->addReference(self::BOOK . $j, $book);
        }
        $manager->flush();
    }

}