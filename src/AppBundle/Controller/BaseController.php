<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Application;
use AppBundle\Entity\Book;
use AppBundle\Entity\Category;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

/**
 * @Route("/{_locale}", requirements = {"_locale" : "en|ru"})
 */
class BaseController extends Controller
{
    /**
     * @Route("/", name="homepage")
     */
    public function indexAction()
    {
        $categories = $this->getDoctrine()->getRepository(Category::class)->findAll();
        $books = $this->getDoctrine()->getRepository(Book::class)->findAll();
        $render_books = [];
        foreach ($books as $book) {
            $status = $this
                ->getDoctrine()
                ->getRepository(Application::class)
                ->getLastApplicationByBookId($book->getId());
            if (!empty($status)) {

                if ($status[0]['status'] == 'Ожидает') {
                    $status = $this->get('translator')->trans('reader.expected') . $status[0]['completionDate']->format('Y-m-d');
                } else {
                    $status = $status[0]['status'];
                }
            } else {
                $status = 'get_book';
            }
            $render_books[] = [
                'book' => $book,
                'status' => $status
            ];
        }
        return $this->render("@App/index.html.twig", [
            'books' => $render_books,
            'categories' => $categories
        ]);
    }

    /**
     * @Route("/show_by_category/{category_id}", name="show_by_category")
     * @param $category_id
     * @return Response
     */
    public function showBookByCategory($category_id)
    {
        $category = $this->getDoctrine()->getRepository(Category::class)->find($category_id);
        $categories = $this->getDoctrine()->getRepository(Category::class)->findAll();
        $render_books = [];
        foreach ($category->getBooks() as $book) {
            $status = $this
                ->getDoctrine()
                ->getRepository(Application::class)
                ->getLastApplicationByBookId($book->getId());
            if (!empty($status)) {

                if ($status[0]['status'] == 'Ожидается') {
                    $status = $this->get('translator')->trans('reader.expected') . $status[0]['completionDate']->format('Y-m-d');
                } else {
                    $status = $status[0]['status'];
                }
            } else {
                $status = 'get_book';
            }
            $render_books[] = [
                'book' => $book,
                'status' => $status
            ];
        }
        return $this->render('@App/index.html.twig', [
            'books' => $render_books,
            'categories' => $categories
        ]);
    }
}
