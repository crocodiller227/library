<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Application;
use AppBundle\Entity\Book;
use AppBundle\Entity\Reader;
use Doctrine\ORM\EntityManagerInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

/**
 * @Route("/{_locale}", requirements = {"_locale" : "en|ru"})
 */
class ReaderController extends Controller
{
    /**
     * @Route("/register_reader", name="register_reader")
     * @param Request $request
     * @param EntityManagerInterface $em
     * @return Response
     */
    public function registerReaderAction(Request $request, EntityManagerInterface $em)
    {
        $form = $this->createFormBuilder()
            ->add(
                'full_name',
                TextType::class,
                ['label' => $this->get('translator')->trans('reader.full_name', [], 'messages')]
            )
            ->add(
                'address',
                TextType::class,
                ['label' => $this->get('translator')->trans('reader.address', [], 'messages')]
            )
            ->add(
                'passport_id',
                TextType::class,
                ['label' => $this->get('translator')->trans('reader.passport_id', [], 'messages')]
            )
            ->add(
                'save',
                SubmitType::class,
                ['label'=>$this->get('translator')->trans('reader.get_reader_card', [], 'messages')]
            )
            ->getForm();
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()){
            $data = $form->getData();
            $reader = new Reader();
            $readerCardId = uniqid();
            $reader
                ->setFullName($data['full_name'])
                ->setAddress($data['address'])
                ->setPassportId($data['passport_id'])
                ->setReaderCardId($readerCardId);
            $em->persist($reader);
            $em->flush();
            return $this->render('@App/reader/register_completed.html.twig', [
                'reader'=>$reader
            ]);
        }
        return $this->render('@App/reader/register.html.twig', [
            'form'=>$form->createView()
        ]);
    }

    /**
     * @Route("/reader_books", name="reader_books")
     */
    public function showReaderBooks(Request $request){
        $form = $this->createFormBuilder()
            ->add('reader_card_id', TextType::class, [
                'label' => $this->get('translator')->trans('reader.reader_card_id')
            ])
            ->add('save', SubmitType::class, [
                'label'=> $this->get('translator')->trans('app.send')
            ])
            ->getForm();
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()){
            $data = $form->getData();
            $reader = $this
                ->getDoctrine()
                ->getRepository(Reader::class)
                ->getReaderByReaderCardId($data['reader_card_id']);
            if ($reader === null){
                echo "<script>alert(\"{$this->get('translator')->trans('reader.not_found')}\")</script>";
                return $this->redirectToRoute('reader_books');
            }
            return $this->render('@App/reader/books.html.twig', [
                'reader' => $reader[0]
            ]);
        }
        return $this->render('@App/reader/reader_login.html.twig', [
            'form' => $form->createView()
        ]);
    }

    /**
     * @Route("/get_book/{book_id}", name="get_book")
     * @param $book_id
     * @param Request $request
     * @param EntityManagerInterface $em
     * @return RedirectResponse | Response
     */
    public  function getBook($book_id, Request $request, EntityManagerInterface $em){
        $book = $this->getDoctrine()->getRepository(Book::class)->getBookById($book_id);
        $form = $this
            ->createFormBuilder()
            ->add('reader_card_id', TextType::class, [
                'label'=>$this->get('translator')->trans('reader.reader_card_id')
            ])
            ->add('completion_date', DateType::class, [
                'label'=> $this->get('translator')->trans('reader.completion_date')
            ])
            ->add('save', SubmitType::class, [
                'label'=>$this->get('translator')->trans('reader.get_book')
            ])
        ->getForm();
        $form->handleRequest($request);
        if ($form->isValid() && $form->isSubmitted()){
            $data = $form->getData();
            $reader = $this
                ->getDoctrine()
                ->getRepository(Reader::class)
                ->getReaderByReaderCardId($data['reader_card_id']);
            if (empty($reader)){
                echo "<script>alert(\"{$this->get('translator')->trans('reader.not_found')}\")</script>";
                return $this->redirectToRoute('get_book', $book_id);
            } else {
                $application = new Application();
                $application
                    ->setReader($reader[0])
                    ->setBook($book[0])
                    ->setRegistrationDate(new \DateTime())
                    ->setCompletionDate($data['completion_date'])
                    ->setStatus('Ожидает');
                $em->persist($application);
                $em->flush();
                }
                return $this->redirectToRoute('homepage');
        }
        dump($book);
        return $this->render('@App/reader/get_book.html.twig',
            [
                'form' => $form->createView(),
                'book' => $book[0]
            ]);
    }
}
