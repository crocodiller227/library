$(document).ready(function () {
    $('.btn_lang').on('click', function () {
        if ($(this).data('locale') === 'ru') {
            window.location.replace(window.location.href.replace(/\/en\//i, '/ru/'));
        } else {
            window.location.replace(window.location.href.replace(/\/ru\//i, '/en/'));
        }
    })
});